﻿using System.Web;
using System.Web.Optimization;

namespace Kaban
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/Content/allscripts").Include(
                "~/Content/Scripts/Respond.js",
                "~/Content/Scripts/Loaders.js",
                "~/Content/Scripts/Bootstrap.js",
                "~/Content/Scripts/JQuery.js"
           ));

            bundles.Add(new StyleBundle("~/Content/allstyles").Include(
                "~/Content/Styles/Bootswatch/yeti.min.css",
                "~/Content/Styles/customize.css",
                "~/Content/Styles/loaders.min.css"
            ));
        }
    }
}
