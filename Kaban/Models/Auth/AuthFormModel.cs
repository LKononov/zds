﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Kaban.Models.Auth
{
    public class AuthFormModel
    {
        [Required(ErrorMessageResourceType = typeof(Resources.Resource),
                 ErrorMessageResourceName = "Button")]
        public virtual string Login { get; set; }

        [Required(ErrorMessage = "Please enter your password")]
        public virtual string Password { get; set; }
    }
}