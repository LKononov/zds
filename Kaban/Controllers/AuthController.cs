﻿using Kaban.App_Code;
using Kaban.Models;
using Kaban.Models.Auth;
using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Kaban.Controllers
{

    public class AuthController : Controller
    {
        public ActionResult Index()
        {
            if (Session["Id"] != null)
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

        [HttpPost]
        public ActionResult Index(AuthFormModel Model)
        {
            if (Session["ID"] != null)
            {
                return RedirectToAction("Index", "Home");
            }
            if (ModelState.IsValid)
            {
                DatabaseModelDataContext db = new DatabaseModelDataContext();
                Users User = (from Users in db.Users
                              where Users.Login == Model.Login select Users).FirstOrDefault();

                if (User != null)
                {
                    if (User.PasswordHash == Security.SHA512(Model.Password))
                    {
                        Session["id"] = Session.SessionID;
                        return RedirectToAction("Index", "Home");
                    }
                    ModelState.AddModelError("PasswordError", "Password incorect");
                }
                ModelState.AddModelError("UserError", "User not found");
            }                
            return View(Model);
        }

        public ActionResult Logout()
        {
            //kill session
            Session.Abandon();
            return RedirectToAction("Index", "Auth");
        }
    }
}