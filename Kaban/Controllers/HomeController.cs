﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kaban.Models;
using Kaban.App_Code;
using static Kaban.App_Code.Filters;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace Kaban.Controllers
{
    [Culture]
    [OnlySigned]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Ajax(string SearchText)
        {
            string SearchParam = SearchText.ToString();
            DatabaseModelDataContext db = new DatabaseModelDataContext();
            List<Users> Search = db.Users.Where(p => p.Name.StartsWith(SearchParam)).Select(p => p).ToList();

            JObject Result = new JObject();
            Result.Add(new JProperty("Result", JToken.FromObject(Search)));
            return Content(Result.ToString(), "application/json");
        }
        public ActionResult ChangeCulture(string lang)
        {
            string returnUrl = Request.UrlReferrer.AbsolutePath;
            // Список культур
            List<string> cultures = new List<string>() { "pl", "en", "ru" };
            if (!cultures.Contains(lang))
            {
                lang = "en";
            }
            // Сохраняем выбранную культуру в куки
            HttpCookie cookie = Request.Cookies["lang"];
            if (cookie != null)
                cookie.Value = lang;   // если куки уже установлено, то обновляем значение
            else
            {
                cookie = new HttpCookie("lang");
                cookie.HttpOnly = false;
                cookie.Value = lang;
                cookie.Expires = DateTime.Now.AddYears(1);
            }
            Response.Cookies.Add(cookie);
            return Redirect(returnUrl);
        }
    }
}